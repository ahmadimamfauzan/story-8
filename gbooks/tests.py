from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import index

# Create your tests here.

class StatusTest(TestCase):
    # url_test
    def test_status_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    #test_fungsi_index
    def test_status_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    #test_pakai_template
    def test_article_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'gbooks/index.html')
