from django.apps import AppConfig


class GbooksConfig(AppConfig):
    name = 'gbooks'
